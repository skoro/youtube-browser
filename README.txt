# YouTube Browser

Features:
- A native Windows application
- Running in a sandbox environment
- Stay on top mode
- TBD: minimize to tray
- TBD: automatic light/dark theme
- TBD: allow navigating youtube links only
- TDB: switch to miniplayer
+ TDB: keep/restore window size and position on the screen
- TDB: save settings
+ TDB: go back (keyboard shortcut)

Main menu:
- refresh page
- home 
- copy link
- zoom in
- zoom out
- zoom reset

Customizations:

Hide sticky top bar:
window.document.getElementById('masthead-container').style.display='none'
window.document.getElementById('chips-wrapper').style.display='none'