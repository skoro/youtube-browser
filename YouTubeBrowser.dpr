program YouTubeBrowser;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {MainForm},
  uConsts in 'uConsts.pas',
  uIniConfig in 'uIniConfig.pas';

{$R *.res}

begin
  try
    Config := TIniConfigRepository.Create(GetAppDir('config.ini'));
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.Title := 'Youtube Browser';
    Application.CreateForm(TMainForm, MainForm);
    Application.Run;
  finally

  end;
end.

