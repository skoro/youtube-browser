unit uConsts;

interface

const
  YOUTUBE_URL = 'https://youtube.com';

type
  TAppTheme = (atBlack, atWhite);

  IConfigRepository = interface
    ['{6BD801D9-0C6E-4822-9FAA-F1825F3B5E2A}']
    function ReadString(const ParamName: string; Default: string = ''): string;
    function ReadBoolean(const ParamName: string;
      Default: boolean = False): boolean;
    function ReadInt(const ParamName: string; Default: integer = 0): integer;
    function ReadDouble(const ParamName: string; Default: Double = 0.0): Double;
    procedure WriteString(const ParamName, Value: string);
    procedure WriteBoolean(const ParamName: string; Value: boolean);
    procedure WriteInt(const ParamName: string; Value: integer);
    procedure WriteDouble(const ParamName: string; Value: double);
    function HasValue(const ParamName: string): Boolean;
  end;

function GetAppDir(const FileName: string = ''): String;

var
  Config: IConfigRepository;

implementation

uses System.IOUtils, Vcl.Forms;

function GetAppDir(const FileName: string): String;
begin
  Result := TPath.Combine(TPath.GetCachePath,
    TPath.GetFileNameWithoutExtension(Application.ExeName));
  if FileName <> '' then
    Result := TPath.Combine(Result, FileName);
end;

end.
