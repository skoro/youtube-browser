unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.ImageList, Vcl.ImgList,
  Vcl.ComCtrls, Vcl.ToolWin, Vcl.TitleBarCtrls, Vcl.StdCtrls, WebView2,
  Winapi.ActiveX, Vcl.Edge, System.Actions, Vcl.ActnList, Vcl.Menus,
  uConsts;

type
  TMainForm = class(TForm)
    TitleBarPanel: TTitleBarPanel;
    tbTitle: TToolBar;
    tbGoBack: TToolButton;
    EdgeBrowser: TEdgeBrowser;
    tbOnTopMode: TToolButton;
    imlTitleIcons24: TImageList;
    ActionList: TActionList;
    actAlwaysOnTop: TAction;
    tbAppMenu: TToolButton;
    pmAppMenu: TPopupMenu;
    actZoomIn: TAction;
    actZoomOut: TAction;
    actResetZoom: TAction;
    miZoomIn: TMenuItem;
    miZoomOut: TMenuItem;
    miResetZoom: TMenuItem;
    actGoBack: TAction;
    actRefresh: TAction;
    miGoBack: TMenuItem;
    N1: TMenuItem;
    miRefresh: TMenuItem;
    imlMenuIcons: TImageList;
    actCopyURL: TAction;
    miCopyURL: TMenuItem;
    N2: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdgeBrowserDocumentTitleChanged(Sender: TCustomEdgeBrowser;
      const ADocumentTitle: string);
    procedure EdgeBrowserHistoryChanged(Sender: TCustomEdgeBrowser);
    procedure EdgeBrowserNavigationCompleted(Sender: TCustomEdgeBrowser;
      IsSuccess: Boolean; WebErrorStatus: TOleEnum);
    procedure EdgeBrowserContainsFullScreenElementChanged
      (Sender: TCustomEdgeBrowser; ContainsFullScreenElement: Boolean);
    procedure EdgeBrowserCreateWebViewCompleted(Sender: TCustomEdgeBrowser;
      AResult: HRESULT);
    procedure actAlwaysOnTopExecute(Sender: TObject);
    procedure actGoBackExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actRefreshExecute(Sender: TObject);
    procedure actZoomInExecute(Sender: TObject);
    procedure actZoomOutExecute(Sender: TObject);
    procedure actResetZoomExecute(Sender: TObject);
    procedure actCopyURLExecute(Sender: TObject);
  private
    { Private declarations }
    FAppDataDir: string;
    FTheme: TAppTheme;
    FBrowserZoom: Double;
    procedure SetTheme(const Value: TAppTheme);
    procedure SetThemeColors(background, foreground, SysButtonsHover: TColor);
    procedure SaveFormConfig;
    procedure LoadFormConfig;
    function GetStayOnTop: Boolean;
    procedure SetStayOnTop(const Value: Boolean);
    function ConfigKey(const AKey: string): string;
    procedure SetBrowserZoom(const Value: Double);
  public
    { Public declarations }
    property Theme: TAppTheme read FTheme write SetTheme;
    property StayOnTop: Boolean read GetStayOnTop write SetStayOnTop;
    property BrowserZoom: Double read FBrowserZoom write SetBrowserZoom;
  end;

var
  MainForm: TMainForm;

implementation

uses System.IOUtils, Vcl.Clipbrd;

const ZOOM_STEP = 0.2;

{$R *.dfm}

procedure TMainForm.actAlwaysOnTopExecute(Sender: TObject);
begin
  with Sender as TAction do
  begin
    Checked := not Checked;
    StayOnTop := Checked;
  end;
end;

procedure TMainForm.actCopyURLExecute(Sender: TObject);
begin
  Clipboard.AsText := EdgeBrowser.LocationURL;
end;

procedure TMainForm.actGoBackExecute(Sender: TObject);
begin
  EdgeBrowser.GoBack;
end;

procedure TMainForm.actRefreshExecute(Sender: TObject);
begin
  EdgeBrowser.Refresh;
end;

procedure TMainForm.actResetZoomExecute(Sender: TObject);
begin
  BrowserZoom := 1.0;
end;

procedure TMainForm.actZoomInExecute(Sender: TObject);
begin
  BrowserZoom := BrowserZoom + ZOOM_STEP;
end;

procedure TMainForm.actZoomOutExecute(Sender: TObject);
begin
  BrowserZoom := BrowserZoom - ZOOM_STEP;
end;

function TMainForm.ConfigKey(const AKey: string): string;
begin
  Result := Name + '.' + AKey;
end;

procedure TMainForm.EdgeBrowserContainsFullScreenElementChanged
  (Sender: TCustomEdgeBrowser; ContainsFullScreenElement: Boolean);
begin
  if ContainsFullScreenElement then
  begin
    BorderStyle := bsNone;
    WindowState := TWindowState.wsMaximized;
    CustomTitleBar.Enabled := False;
  end
  else
  begin
    WindowState := TWindowState.wsNormal;
    BorderStyle := bsSizeable;
    CustomTitleBar.Enabled := True;
  end;
end;

procedure TMainForm.EdgeBrowserCreateWebViewCompleted
  (Sender: TCustomEdgeBrowser; AResult: HRESULT);
begin
  if Succeeded(AResult) then
  begin
    EdgeBrowser.DefaultContextMenusEnabled := False;
  end;
end;

procedure TMainForm.EdgeBrowserDocumentTitleChanged(Sender: TCustomEdgeBrowser;
  const ADocumentTitle: string);
begin
  Caption := ADocumentTitle;
end;

procedure TMainForm.EdgeBrowserHistoryChanged(Sender: TCustomEdgeBrowser);
begin
  actGoBack.Enabled := EdgeBrowser.CanGoBack;
end;

procedure TMainForm.EdgeBrowserNavigationCompleted(Sender: TCustomEdgeBrowser;
  IsSuccess: Boolean; WebErrorStatus: TOleEnum);
begin
  EdgeBrowser.ZoomFactor := FBrowserZoom;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveFormConfig;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Theme := atBlack;
  LoadFormConfig;
  actAlwaysOnTop.Checked := StayOnTop;
  FAppDataDir := GetAppDir;
  Caption := '';
  tbGoBack.Enabled := False;
  EdgeBrowser.UserDataFolder := TPath.Combine(FAppDataDir, 'UserData');
  EdgeBrowser.Navigate(YOUTUBE_URL);
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  //
end;

function TMainForm.GetStayOnTop: Boolean;
begin
  Result := (FormStyle = fsStayOnTop);
end;

procedure TMainForm.LoadFormConfig;
begin
  with Config do
  begin
    Left := ReadInt(ConfigKey('Left'), 0);
    Top := ReadInt(ConfigKey('Top'), 0);
    Width := Abs(ReadInt(ConfigKey('Width'), 800));
    Height := Abs(ReadInt(ConfigKey('Height'), 600));
    if Left < 0 then Left := 0;
    if Top < 0 then Top := 0;
    if Left > (Screen.DesktopWidth - 16) then Left := Abs(Screen.DesktopWidth - Width);
    if Top > (Screen.DesktopHeight - 4) then Top := Abs(Screen.DesktopHeight - Height);
    StayOnTop := ReadBoolean(ConfigKey('StayOnTop'), False);
    Theme := TAppTheme(ReadInt(ConfigKey('Theme'), Ord(atWhite)));
    BrowserZoom := ReadDouble(ConfigKey('BrowserZoom'), 1.0);
  end;
end;

procedure TMainForm.SaveFormConfig;
begin
  with Config do
  begin
    WriteInt(ConfigKey('Left'), Left);
    WriteInt(ConfigKey('Top'), Top);
    WriteInt(ConfigKey('Width'), Width);
    WriteInt(ConfigKey('Height'), Height);
    WriteBoolean(ConfigKey('StayOnTop'), StayOnTop);
    WriteInt(ConfigKey('Theme'), Ord(FTheme));
    WriteDouble(ConfigKey('BrowserZoom'), BrowserZoom);
  end;
end;

procedure TMainForm.SetBrowserZoom(const Value: Double);
begin
  EdgeBrowser.ZoomFactor := Value;
  FBrowserZoom := Value;
end;

procedure TMainForm.SetStayOnTop(const Value: Boolean);
begin
  case Value of
    True: FormStyle := fsStayOnTop;
    False: FormStyle := fsNormal;
  end;
end;

procedure TMainForm.SetTheme(const Value: TAppTheme);
begin
  case Value of
    atBlack:
      SetThemeColors(clBlack, clWhite, clGray);
    atWhite:
      SetThemeColors(clWhite, clBlack, $00F4F4F4);
  else
    raise Exception.Create('Unsupported theme');
  end;
  FTheme := Value;
end;

procedure TMainForm.SetThemeColors(background, foreground,
  SysButtonsHover: TColor);
begin
  with CustomTitleBar do
  begin
    // Do not apply custom colors when system ones are used.
    if (Enabled and SystemColors) then
      Exit;

    ButtonBackgroundColor := background;
    ButtonForegroundColor := foreground;
    ButtonInactiveBackgroundColor := background;
    ButtonHoverBackgroundColor := SysButtonsHover;
    BackgroundColor := background;
    InactiveBackgroundColor := background;
    ForegroundColor := foreground
  end;

  if not tbTitle.Transparent then
  begin
    tbTitle.GradientStartColor := background;
    tbTitle.GradientEndColor := background;
  end;

end;

end.
