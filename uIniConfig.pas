unit uIniConfig;

interface

uses uConsts, System.IniFiles;

type

  TIniConfigRepository = class(TInterfacedObject, IConfigRepository)
  private
    FSection: String;
    FIni: TIniFile;
  public
    constructor Create(const ConfigFileName: string);
    destructor Destroy; override;
    function ReadString(const ParamName: string; Default: string = ''): string;
    function ReadBoolean(const ParamName: string;
      Default: boolean = False): boolean;
    function ReadInt(const ParamName: string; Default: integer = 0): integer;
    function ReadDouble(const ParamName: string; Default: Double = 0.0): Double;
    procedure WriteString(const ParamName, Value: string);
    procedure WriteBoolean(const ParamName: string; Value: boolean);
    procedure WriteInt(const ParamName: string; Value: integer);
    procedure WriteDouble(const ParamName: string; Value: double);
    function HasValue(const ParamName: string): Boolean;
  end;

implementation

{ TIniConfigRepository }

constructor TIniConfigRepository.Create(const ConfigFileName: string);
begin
  inherited Create;
  FSection := 'Config';
  FIni := TIniFile.Create(ConfigFileName);
end;

destructor TIniConfigRepository.Destroy;
begin
  FIni.Free;
  inherited;
end;

function TIniConfigRepository.HasValue(const ParamName: string): Boolean;
begin
  Result := FIni.ValueExists(FSection, ParamName);
end;

function TIniConfigRepository.ReadBoolean(const ParamName: string;
  Default: boolean): boolean;
begin
  Result := FIni.ReadBool(FSection, ParamName, Default);
end;

function TIniConfigRepository.ReadDouble(const ParamName: string;
  Default: Double): Double;
begin
  Result := FIni.ReadFloat(FSection, ParamName, Default);
end;

function TIniConfigRepository.ReadInt(const ParamName: string; Default: integer): integer;
begin
  Result := FIni.ReadInteger(FSection, ParamName, Default);
end;

function TIniConfigRepository.ReadString(const ParamName: string;
  Default: string): string;
begin
  Result := FIni.ReadString(FSection, ParamName, Default);
end;

procedure TIniConfigRepository.WriteBoolean(const ParamName: string;
  Value: boolean);
begin
  FIni.WriteBool(FSection, ParamName, Value);
end;

procedure TIniConfigRepository.WriteDouble(const ParamName: string;
  Value: double);
begin
  FIni.WriteFloat(FSection, ParamName, Value);
end;

procedure TIniConfigRepository.WriteInt(const ParamName: string;
  Value: integer);
begin
  FIni.WriteInteger(FSection, ParamName, Value);
end;

procedure TIniConfigRepository.WriteString(const ParamName, Value: string);
begin
  FIni.WriteString(FSection, ParamName, Value);
end;

end.
